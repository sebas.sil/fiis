declare var global: {
  ReanimatedDataMock: {now: () => number}
  fetch(input: any): Promise<any>
}
declare module '*.html' {
  const value: string
  export default value
}
declare var performance: {
  now(): DOMHighResTimeStamp
}
