module.exports = {
  root: true,
  extends: '@react-native-community',
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  overrides: [
    {
      files: ['*.ts', '*.tsx', '*.js'],
      rules: {
        'prettier/prettier': 'error',
        '@typescript-eslint/no-shadow': ['error'],
        'no-shadow': 'off',
        'no-undef': 'off',
        semi: ['error', 'never', {beforeStatementContinuationChars: 'never'}],
        'comma-dangle': ['error', 'never'],
        '@typescript-eslint/comma-dangle': 'off',
        'jsx-quotes': ['error', 'prefer-single'],
        'react-hooks/exhaustive-deps': 'warn',
        'no-console': ['error', {allow: ['error']}]
      }
    }
  ]
}
