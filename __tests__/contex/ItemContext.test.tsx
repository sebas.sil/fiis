import 'react-native'
import React from 'react'
import {act, renderHook} from '@testing-library/react-native'
import {useItem, ItemProvider} from '../../src/context/ItemContext'
import {Fii} from '../../src/component/Item'

//BUG https://github.com/nock/nock/issues/2200
//jest.useFakeTimers('modern').setSystemTime(new Date('2022-08-31 03:22:10').getTime())

let fii1: Fii
let fii2: Fii

beforeEach(() => {
  fii1 = {
    acronym: 'BCFF11',
    name: 'FII BTG PACTUAL FUNDO DE FUNDOS',
    pvp: 0.95,
    dy: 8.7,
    liquidity: 103000,
    created: new Date('01/01/2022')
  }
  fii2 = {
    acronym: 'CPTS11',
    name: 'CAPITÂNIA SECURITIES II FUNDO DE INVESTIMENTO IMOBILIÁRIO - FII',
    pvp: 0.81,
    dy: 0.71,
    liquidity: 3000,
    created: new Date('01/01/2022')
  }
})

describe('<ItemContext />', () => {
  it('execute setFii function', async () => {
    const wrapper = ({children}: any) => <ItemProvider>{children}</ItemProvider>
    const {result} = renderHook(() => useItem(), {wrapper})
    act(() => {
      result.current.setFiis([fii1, fii2])
    })
    expect(result.current.fiis).toMatchObject([fii1, fii2])
  })
})
