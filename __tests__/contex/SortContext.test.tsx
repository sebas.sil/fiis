import 'react-native'
import React from 'react'
import {act, renderHook} from '@testing-library/react-native'
import {useSort, SortProvider} from '../../src/context/SortContext'
import {Sort} from '../../src/component/HeaderList/FilterCell'
import {Fii} from '../../src/component/Item'

//BUG https://github.com/nock/nock/issues/2200
//jest.useFakeTimers('modern').setSystemTime(new Date('2022-08-31 03:22:10').getTime())

let fii1: Fii
let fii2: Fii

beforeEach(() => {
  fii1 = {
    acronym: 'BCFF11',
    name: 'FII BTG PACTUAL FUNDO DE FUNDOS',
    pvp: 0.95,
    dy: 8.7,
    liquidity: 103000,
    created: new Date('01/01/2022')
  }
  fii2 = {
    acronym: 'CPTS11',
    name: 'CAPITÂNIA SECURITIES II FUNDO DE INVESTIMENTO IMOBILIÁRIO - FII',
    pvp: 0.81,
    dy: 0.71,
    liquidity: 3000,
    created: new Date('01/01/2022')
  }
})

describe('<SortContext />', () => {
  it('execute sort function < as DESC', async () => {
    const wrapper = ({children}: any) => <SortProvider>{children}</SortProvider>
    const {result} = renderHook(() => useSort(), {wrapper})
    act(() => {
      result.current.setOrder(Sort.DESC)
      result.current.setField('dy')
    })
    const response = result.current.sortFunction(fii1, fii2)
    expect(response).toBe(-1)
  })

  it('execute sort function > and DESC', async () => {
    const wrapper = ({children}: any) => <SortProvider>{children}</SortProvider>
    const {result} = renderHook(() => useSort(), {wrapper})
    act(() => {
      result.current.setOrder(Sort.DESC)
      result.current.setField('dy')
    })
    const response = result.current.sortFunction(fii2, fii1)
    expect(response).toBe(1)
  })

  it('execute sort function < as ASC', async () => {
    const wrapper = ({children}: any) => <SortProvider>{children}</SortProvider>
    const {result} = renderHook(() => useSort(), {wrapper})
    act(() => {
      result.current.setOrder(Sort.ASC)
      result.current.setField('dy')
    })
    const response = result.current.sortFunction(fii1, fii2)
    expect(response).toBe(1)
  })

  it('execute sort function > and ASC', async () => {
    const wrapper = ({children}: any) => <SortProvider>{children}</SortProvider>
    const {result} = renderHook(() => useSort(), {wrapper})
    act(() => {
      result.current.setOrder(Sort.ASC)
      result.current.setField('dy')
    })
    const response = result.current.sortFunction(fii2, fii1)
    expect(response).toBe(-1)
  })

  it('execute sort function =', async () => {
    const wrapper = ({children}: any) => <SortProvider>{children}</SortProvider>
    const {result} = renderHook(() => useSort(), {wrapper})
    act(() => result.current.setField('dy'))
    const response = result.current.sortFunction(fii1, fii1)
    expect(response).toBe(0)
  })

  it('execute sort function = because it cant sort (field is undefined)', async () => {
    const wrapper = ({children}: any) => <SortProvider>{children}</SortProvider>
    const {result} = renderHook(() => useSort(), {wrapper})
    const response = result.current.sortFunction(fii1, fii2)
    expect(response).toBe(0)
  })

  it('execute sort function > because a is undefined', async () => {
    const wrapper = ({children}: any) => <SortProvider>{children}</SortProvider>
    const {result} = renderHook(() => useSort(), {wrapper})
    const tmp = {...fii1, dy: undefined}
    act(() => result.current.setField('dy'))
    const response = result.current.sortFunction(tmp, fii2)
    expect(response).toBe(1)
  })

  it('execute sort function < because b is undefined', async () => {
    const wrapper = ({children}: any) => <SortProvider>{children}</SortProvider>
    const {result} = renderHook(() => useSort(), {wrapper})
    const tmp = {...fii2, dy: undefined}
    act(() => result.current.setField('dy'))
    const response = result.current.sortFunction(fii1, tmp)
    expect(response).toBe(-1)
  })
})
