import 'react-native'
import React from 'react'
import {act, renderHook} from '@testing-library/react-native'
import {useFilter, FilterProvider} from '../../src/context/FilterContext'

describe('<FilterContext />', () => {
  it('execute filter function dy', async () => {
    const wrapper = ({children}: any) => <FilterProvider>{children}</FilterProvider>
    const {result} = renderHook(() => useFilter(), {wrapper})
    act(() => {
      result.current.setField('dy', {min: 1, max: 2})
    })
    expect(result.current.created).toBeUndefined()
    expect(result.current.liquidity).toBeUndefined()
    expect(result.current.pvp).toBeUndefined()
    expect(result.current.dy).toStrictEqual({min: 1, max: 2})
  })

  it('execute filter function pvp', async () => {
    const wrapper = ({children}: any) => <FilterProvider>{children}</FilterProvider>
    const {result} = renderHook(() => useFilter(), {wrapper})
    act(() => {
      result.current.setField('pvp', {min: 1, max: 2})
    })
    expect(result.current.created).toBeUndefined()
    expect(result.current.liquidity).toBeUndefined()
    expect(result.current.pvp).toStrictEqual({min: 1, max: 2})
    expect(result.current.dy).toBeUndefined()
  })

  it('execute filter function liquidity', async () => {
    const wrapper = ({children}: any) => <FilterProvider>{children}</FilterProvider>
    const {result} = renderHook(() => useFilter(), {wrapper})
    act(() => {
      result.current.setField('liquidity', {min: 1, max: 2})
    })
    expect(result.current.created).toBeUndefined()
    expect(result.current.liquidity).toStrictEqual({min: 1, max: 2})
    expect(result.current.pvp).toBeUndefined()
    expect(result.current.dy).toBeUndefined()
  })

  it('execute filter function created', async () => {
    const wrapper = ({children}: any) => <FilterProvider>{children}</FilterProvider>
    const {result} = renderHook(() => useFilter(), {wrapper})
    act(() => {
      result.current.setField('acronym', 'MXRF')
    })
    expect(result.current.acronym).toBe('MXRF')
    expect(result.current.liquidity).toBeUndefined()
    expect(result.current.pvp).toBeUndefined()
    expect(result.current.dy).toBeUndefined()
  })

  it('execute filter function acronym', async () => {
    const wrapper = ({children}: any) => <FilterProvider>{children}</FilterProvider>
    const {result} = renderHook(() => useFilter(), {wrapper})
    let dt1 = new Date('2022-10-03')
    let dt2 = new Date('2022-10-11')
    act(() => {
      result.current.setField('created', {min: dt1.getTime(), max: dt2.getTime()})
    })
    expect(result.current.created).toMatchObject({min: 1664755200000, max: 1665446400000})
    expect(result.current.liquidity).toBeUndefined()
    expect(result.current.pvp).toBeUndefined()
    expect(result.current.dy).toBeUndefined()
  })
})
