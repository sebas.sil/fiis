import {getAllFii} from '../../src/service/B3ServiceHandle'
import data from '../b3.data.json'

beforeEach(() => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve(data)
    })
  )
})

describe('B3ServiceHandler', () => {
  it('call an external url and translate to Fii object list', async () => {
    const list = await getAllFii()
    expect(list).toHaveLength(2)
    expect(list[0].acronym).toBe('CCME11')
    expect(list[0].name).toBe('FII CANUMA')
    expect(list[0].dy).toBeUndefined()
    expect(list[0].pvp).toBeUndefined()
    expect(list[0].created).toBeUndefined()
    expect(list[0].liquidity).toBeUndefined()
  })

  it('should catch a trasnform error', async () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve()
      })
    )
    expect(() => getAllFii()).rejects.toThrowError()
    expect(() => getAllFii()).rejects.toMatch(/cant parse Fundamentus:/)
  })

  it('shouldnt execute when disabled', async () => {
    const list = await getAllFii(false)
    expect(list).toHaveLength(0)
  })
})
