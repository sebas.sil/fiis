import {getAllFii} from '../../src/service/StatusInvestServiceHandle'
import data from '../statusinvest.data.json'

beforeEach(() => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve(data)
    })
  )
})

describe('StatusInvestServiceHandle', () => {
  it('call an external url and translate to Fii object list', async () => {
    const list = await getAllFii()
    expect(list).toHaveLength(5)
    expect(list[0].acronym).toBe('VGHF11')
    expect(list[0].name).toBe('VALORA HEDGE FUND')
    expect(list[0].dy).toBe(17.3428)
    expect(list[0].pvp).toBe(1.0625)
    expect(list[0].created).toBeUndefined()
    expect(list[0].liquidity).toBeUndefined()
  })

  it('should catch a trasnform error', async () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve()
      })
    )
    expect(() => getAllFii()).rejects.toThrowError()
    expect(() => getAllFii()).rejects.toMatch(/cant parse Fundamentus:/)
  })

  it('shouldnt execute when disabled', async () => {
    const list = await getAllFii(false)
    expect(list).toHaveLength(0)
  })
})
