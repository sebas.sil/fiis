import {getAllFii} from '../../src/service/FundamentusHandle'
import data from '../fundamentus.data.html'

beforeEach(() => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      text: () => Promise.resolve(data)
    })
  )
})
describe('FundamentusServiceHandler', () => {
  it('call an external url and translate to Fii object list', async () => {
    const list = await getAllFii()
    expect(list).toHaveLength(2)
    expect(list[0].acronym).toBe('BCRI11')
    expect(list[0].name).toBe('BANESTES RECEBIVEIS IMOBILIARIOS FII')
    expect(list[0].dy).toBe(14.76)
    expect(list[0].pvp).toBe(0.97)
    expect(list[0].created).toBeUndefined()
    expect(list[0].liquidity).toBe(1005.32)
  })

  it('call an external url and unable to translate to Fii object list', async () => {
    const list = await getAllFii()
    expect(list).toHaveLength(2)
    expect(list[1].acronym).toBe('?1')
    expect(list[1].name).toBeUndefined()
    expect(list[1].dy).toBeUndefined()
    expect(list[1].pvp).toBeUndefined()
    expect(list[1].created).toBeUndefined()
    expect(list[1].liquidity).toBeUndefined()
  })

  it('should catch a trasnform error', async () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        text: () => Promise.resolve()
      })
    )
    expect(() => getAllFii()).rejects.toThrowError()
    expect(() => getAllFii()).rejects.toMatch(/cant parse Fundamentus:/)
  })

  it('shouldnt execute when disabled', async () => {
    const list = await getAllFii(false)
    expect(list).toHaveLength(0)
  })
})
