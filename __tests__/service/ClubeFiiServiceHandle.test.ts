import {getAllFii} from '../../src/service/ClubeFiiServiceHandle'
import data from '../clubefii.data.html'

beforeEach(() => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      text: () => Promise.resolve(data)
    })
  )
})
describe('ClubeFiiServiceHandler', () => {
  it('call an external url and translate to Fii object list', async () => {
    const list = await getAllFii()
    expect(list).toHaveLength(4)
    expect(list[0].acronym).toBe('AFHI11')
    expect(list[0].name).toBe('AF Invest CRI')
    expect(list[0].dy).toBeUndefined()
    expect(list[0].pvp).toBeUndefined()
    expect(list[0].created).toMatchObject(new Date('2021-04-05'))
    expect(list[0].liquidity).toBeUndefined()
  })

  it('call an external url and unable to translate to Fii object list', async () => {
    const list = await getAllFii()
    expect(list).toHaveLength(4)
    expect(list[1].acronym).toBe('?1')
    expect(list[1].name).toBeUndefined()
    expect(list[1].dy).toBeUndefined()
    expect(list[1].pvp).toBeUndefined()
    expect(list[1].created).toBeUndefined()
    expect(list[1].liquidity).toBeUndefined()
  })

  it('should catch a trasnform error', async () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        text: () => Promise.resolve()
      })
    )
    expect(() => getAllFii()).rejects.toThrowError()
    expect(() => getAllFii()).rejects.toMatch(/cant parse ClubeFii:/)
  })

  it('should catch a date transform error', async () => {
    const list = await getAllFii()
    expect(list).toHaveLength(4)

    expect(list[2].acronym).toBe('AGRX11')
    expect(list[2].created).toBeUndefined()
  })

  it('shouldnt execute when disabled', async () => {
    const list = await getAllFii(false)
    expect(list).toHaveLength(0)
  })
})
