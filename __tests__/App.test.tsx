import 'react-native'
import React from 'react'
import {App} from '../src/App'
import {render, screen} from '@testing-library/react-native'
import {Text} from 'react-native'

jest.mock('../src/MainComponent', () => ({
  MainComponent: (props: any) => <MockedComponentWithText {...props} />
}))
jest.mock('../src/component/Header', () => ({
  Header: (props: any) => <MockedComponent {...props} />
}))
jest.mock('../src/component/Settings', () => ({
  Settings: (props: any) => <MockedComponent {...props} />
}))
jest.mock('../src/context/ItemContext', () => ({
  ItemContext: (props: any) => <MockedComponent {...props} />,
  ItemProvider: (props: any) => <MockedComponent {...props} />
}))
jest.mock('../src/context/SortContext', () => ({
  SortContext: (props: any) => <MockedComponent {...props} />,
  SortProvider: (props: any) => <MockedComponent {...props} />
}))
jest.mock('../src/context/FilterContext', () => ({
  FilterContext: (props: any) => <MockedComponent {...props} />,
  FilterProvider: (props: any) => <MockedComponent {...props} />
}))

const MockedComponent = ({children}: any) => {
  return <>{children}</>
}

const MockedComponentWithText = () => (
  <MockedComponent>
    <Text>Nothing to show</Text>
  </MockedComponent>
)

describe('<App />', () => {
  it('renders the App component with an empty list', async () => {
    render(<App />)
    const item = screen.getByText(/Nothing to show/i)
    expect(item).toBeTruthy()
  })
})
