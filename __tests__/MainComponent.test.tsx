import 'react-native'
import React, {useEffect} from 'react'
import {MainComponent} from '../src/MainComponent'
import {act, fireEvent, render, screen} from '@testing-library/react-native'
import {SortProvider} from '../src/context/SortContext'
import data from './fiis.data.json'
import * as serviceFd from '../src/service/FundamentusHandle'
import * as serviceB3 from '../src/service/B3ServiceHandle'
import * as serviceClub from '../src/service/ClubeFiiServiceHandle'
import * as statusInvest from '../src/service/StatusInvestServiceHandle'
import {FlatList} from 'react-native'
import {ItemProvider, useItem} from '../src/context/ItemContext'
import {FilterProvider, useFilter} from '../src/context/FilterContext'

//FIXME spy on getFiis of MainComponent instead of services
jest.spyOn(serviceFd, 'getAllFii').mockReturnValue(
  new Promise(res =>
    res(
      data.map(e => ({
        ...e,
        created: new Date(e.created)
      }))
    )
  )
)
jest.spyOn(serviceB3, 'getAllFii').mockReturnValue(
  new Promise(res => {
    const arr = data.map(e => ({
      ...e,
      created: new Date(e.created)
    }))
    arr.push({
      acronym: 'HCTR11',
      name: 'HECTARE CE'
    } as any)
    res(arr)
  })
)
jest.spyOn(serviceClub, 'getAllFii').mockReturnValue(
  new Promise(res =>
    res(
      data.map(e => ({
        ...e,
        created: new Date(e.created)
      }))
    )
  )
)
jest.spyOn(statusInvest, 'getAllFii').mockReturnValue(
  new Promise(res =>
    res(
      data.map(e => ({
        ...e,
        created: new Date(e.created)
      }))
    )
  )
)
let wrapper: React.FC

beforeEach(() => {
  wrapper = ({children}: any) => (
    <ItemProvider>
      <FilterProvider>{children}</FilterProvider>
    </ItemProvider>
  )
})

it('renders the Main component with an empty list and without sort', async () => {
  render(
    <SortProvider>
      <MainComponent />
    </SortProvider>,
    {wrapper}
  )
  const button = screen.getByText(/acronym/i)
  await act(() => fireEvent.press(button))
})

it('renders the Main component with list after click on Recarregar', async () => {
  render(
    <SortProvider>
      <MainComponent />
    </SortProvider>,
    {wrapper}
  )
  const button = screen.getByText(/Recarregar/i)
  await act(() => fireEvent.press(button))
  const item = screen.getByText(/MXRF11/i)
  expect(item).toBeTruthy()
})

it('renders the Main component with an empty list', async () => {
  render(<MainComponent />, {wrapper})
  const item = screen.getByText(/Nothing to show/i)
  expect(item).toBeTruthy()
})

it('should render de component and filter by field dy', async () => {
  const TestingComponent = () => {
    const {setFiis} = useItem()
    const {setField} = useFilter()

    useEffect(() => {
      setFiis(data.map(e => ({...e, created: undefined})))
      setField('dy', {min: 0, max: 1})
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return <MainComponent />
  }
  render(<TestingComponent />, {wrapper})
})

it('should render de component and filter by field pvp', async () => {
  const TestingComponent = () => {
    const {setFiis} = useItem()
    const {setField} = useFilter()

    useEffect(() => {
      setFiis(data.map(e => ({...e, created: undefined})))
      setField('pvp', {min: 0, max: 1})
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return <MainComponent />
  }
  render(<TestingComponent />, {wrapper})
})

it('should render de component and filter by field liquidity', async () => {
  const TestingComponent = () => {
    const {setFiis} = useItem()
    const {setField} = useFilter()

    useEffect(() => {
      setFiis(data.map(e => ({...e, created: undefined})))
      setField('liquidity', {min: 0, max: 1})
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return <MainComponent />
  }
  render(<TestingComponent />, {wrapper})
})

it('should render de component and filter by field created', async () => {
  const TestingComponent = () => {
    const {setFiis} = useItem()
    const {setField} = useFilter()

    useEffect(() => {
      setFiis(data.map(e => ({...e, created: undefined})))
      setField('created', {min: 0, max: 1})
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return <MainComponent />
  }
  render(<TestingComponent />, {wrapper})
})

it('should render de component and filter by field acronym', async () => {
  const TestingComponent = () => {
    const {setFiis} = useItem()
    const {setField} = useFilter()

    useEffect(() => {
      setFiis(data.map(e => ({...e, created: undefined})))
      setField('acronym', 'MXRF')
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return <MainComponent />
  }
  render(<TestingComponent />, {wrapper})
})

it('should be well configurated for a good performance', async () => {
  const {UNSAFE_getByType} = render(<MainComponent />, {wrapper})
  const flatlist = UNSAFE_getByType(FlatList)
  expect(flatlist).toBeTruthy()
  expect(flatlist.props.maxToRenderPerBatch).toBe(12)
  //TODO test expect(flatlist.props.getItemLayout)
})
