import 'react-native'
import React from 'react'
import {Item} from '../../src/component/Item'
import {render, screen} from '@testing-library/react-native'

jest.useFakeTimers('modern').setSystemTime(new Date('2022-08-31 03:22:10').getTime())

it('renders the Item component', () => {
  render(
    <Item
      acronym='KNRI11'
      name='KINEA RENDA IMOBILIÁRIA FDO INV IMOB'
      dy={0.67}
      pvp={0.96}
      created={new Date()}
      liquidity={103000}
    />
  )
  expect(screen.getByText(/KNRI11/i)).toBeTruthy()
  expect(screen.getByText(/KINEA RENDA IMOBILIÁRIA FDO INV IMOB/i)).toBeTruthy()
  expect(screen.getByText(/0.67/i)).toBeTruthy()
  expect(screen.getByText(/0.96/i)).toBeTruthy()
  expect(screen.getByText(/31\/08\/2022/i)).toBeTruthy()
})
