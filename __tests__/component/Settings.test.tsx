import 'react-native'
import React, {useEffect} from 'react'
import {render, screen} from '@testing-library/react-native'
import {Settings} from '../../src/component/Settings'
import {ItemProvider, useItem} from '../../src/context/ItemContext'
import data from './../fiis.data.json'

it('renders the Settings component with empty fii list', () => {
  const wrapper = ({children}: any) => <ItemProvider>{children}</ItemProvider>
  render(<Settings />, {wrapper})
  expect(screen.getAllByText(/1.000/)).toHaveLength(1)
  expect(screen.getAllByText(/^0$/)).toHaveLength(3)
})

it('renders the Settings component with fii list', () => {
  const wrapper = ({children}: any) => <ItemProvider>{children}</ItemProvider>
  render(<TestingComponent />, {wrapper})
  expect(screen.getByText(/0,7/)).toBeTruthy() // min dy
  expect(screen.getByText(/8,7/)).toBeTruthy() // max dy
  expect(screen.getByText(/^1$/)).toBeTruthy() // min pvp 0.95 rounded up
  expect(screen.getByText(/0,8/)).toBeTruthy() // max pvp
  expect(screen.getByText(/^0$/)).toBeTruthy() // min liquity
  expect(screen.getByText(/1.000/)).toBeTruthy() // max liquity
})

const TestingComponent = () => {
  const {setFiis} = useItem()

  useEffect(() => {
    setFiis(data.map(e => ({...e, created: undefined})))
  }, [setFiis])
  return <Settings />
}
