import 'react-native'
import React from 'react'
import {act, render, screen} from '@testing-library/react-native'
import {SettingField} from '../../src/component/Settings/SettingField'
import {FilterProvider} from '../../src/context/FilterContext'
import MultiSlider from '@ptomasroos/react-native-multi-slider'

// const mockedContext = jest.mock('../../src/context/FilterContext', () => ({
//   FilterContext: (props: any) => <MockedComponent {...props} />,
//   FilterProvider: (props: any) => <MockedComponent {...props} />,
//   setField: jest.fn()
// }))

// const MockedComponent = ({children}: any) => {
//   return <>{children}</>
// }

it('renders the Setting Field component', () => {
  render(<SettingField minValue={0} maxValue={1.2} label='dy' type='number' step={0.2} />)
  expect(screen.getByText(/dy/i)).toBeTruthy()
  expect(screen.getByText(/0/i)).toBeTruthy()
  expect(screen.getByText(/1,2/i)).toBeTruthy()
})

it('renders the Setting Field component and drag the min value', () => {
  const {UNSAFE_getByType} = render(
    <FilterProvider>
      <SettingField minValue={5} maxValue={30} label='dy' type='number' step={0.2} />
    </FilterProvider>
  )
  //TODO change to fireGestureHandler
  const multiSlider = UNSAFE_getByType(MultiSlider)
  act(() => {
    multiSlider.props.onValuesChange(() => [10, 23])
  })
  //expect(mockedContext.mock.arguments).toBeCalledTimes(1)
})
