import 'react-native'
import React from 'react'
import {FilterCell, Sort} from '../../src/component/HeaderList/FilterCell'
import {render, screen} from '@testing-library/react-native'

jest.useFakeTimers('modern').setSystemTime(new Date('2022-08-31 03:22:10').getTime())

describe('<FilterCell />', () => {
  it('renders the FilterCell component', () => {
    render(<FilterCell label={['created']} />)
    expect(screen.getByText(/created/i)).toBeTruthy()
  })

  it('renders the FilterCell component sortered asc', () => {
    render(<FilterCell label={['created']} ordered={Sort.ASC} />)
    expect(screen.getByText(/created/i)).toBeTruthy()
    expect(screen.getByText(/▲/i)).toBeTruthy()
  })

  it('renders the FilterCell component sortered desc', () => {
    render(<FilterCell label={['created']} ordered={Sort.DESC} />)
    expect(screen.getByText(/created/i)).toBeTruthy()
    expect(screen.getByText(/▼/i)).toBeTruthy()
  })
})
