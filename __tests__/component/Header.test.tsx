import 'react-native'
import React, {useEffect} from 'react'
import {Header} from '../../src/component/Header'
import {act, fireEvent, render, screen} from '@testing-library/react-native'
import {FilterProvider, useFilter} from '../../src/context/FilterContext'
import {ItemProvider, useItem} from '../../src/context/ItemContext'
import data from '../fiis.data.json'

jest.mock('@react-navigation/drawer', () => ({
  DrawerToggleButton: (props: any) => <MockedComponent {...props} />
}))

const MockedComponent = ({children}: any) => {
  return <>{children}</>
}

describe('<Header />', () => {
  it('renders the Header component without filter an ditens', () => {
    render(
      <ItemProvider>
        <FilterProvider>
          <Header />
        </FilterProvider>
      </ItemProvider>
    )
    expect(screen.getByText(/0\/0/i)).toBeTruthy()
    expect(screen.getByPlaceholderText(/Busque pela tag/i)).toBeTruthy()
  })

  it('renders the Header component with filter and item', () => {
    const TestingComponent = () => {
      const {setFiis} = useItem()
      const {setFiltered} = useFilter()

      useEffect(() => {
        setFiis(data.map(e => ({...e, created: undefined})))
        setFiltered(data.map(e => ({...e, created: undefined})).slice(1, 3))
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, [])
      return <Header />
    }

    render(
      <ItemProvider>
        <FilterProvider>
          <TestingComponent />
        </FilterProvider>
      </ItemProvider>
    )
    expect(screen.getByText(/2\/13/i)).toBeTruthy()
    expect(screen.getByPlaceholderText(/Busque pela tag/i)).toBeTruthy()
  })

  it('renders the Header component with acronym filtering', async () => {
    const TestingComponent = () => {
      const {setFiis} = useItem()
      const {setFiltered} = useFilter()

      useEffect(() => {
        setFiis(data.map(e => ({...e, created: undefined})))
        setFiltered(data.map(e => ({...e, created: undefined})).slice(1, 3))
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, [])
      return <Header />
    }

    render(
      <ItemProvider>
        <FilterProvider>
          <TestingComponent />
        </FilterProvider>
      </ItemProvider>
    )
    await act(() => fireEvent.changeText(screen.getByPlaceholderText(/Busque pela tag/i), 'MXRF'))
    expect(screen.getByText(/2\/13/i)).toBeTruthy()
    expect(screen.getByPlaceholderText(/Busque pela tag/i)).toBeTruthy()
    //TODO verify onChangeText by mocking
  })
})
