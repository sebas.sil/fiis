import 'react-native'
import React from 'react'
import {HeaderList} from '../../src/component/HeaderList'
import {fireEvent, render, screen, within} from '@testing-library/react-native'
import {act} from 'react-test-renderer'
import {SortProvider} from '../../src/context/SortContext'

describe('<HeaderList />', () => {
  it('renders the HeaderList component without sorted column', () => {
    render(<HeaderList />)
    expect(screen.getByText(/acronym/i)).toBeTruthy()
    expect(screen.getByText(/name/i)).toBeTruthy()
    expect(screen.getByText(/pvp/i)).toBeTruthy()
    expect(screen.getByText(/dy/i)).toBeTruthy()
    expect(screen.getByText(/created/i)).toBeTruthy()
  })

  it('renders the HeaderList component with acronym sorted asc column', async () => {
    render(
      <SortProvider>
        <HeaderList />
      </SortProvider>
    )

    const button = screen.getByText(/acronym/i)!.parent!.parent!.parent
    await act(() => fireEvent.press(button))
    const acronym = within(button)
    expect(acronym.queryByText('▲')).toBeTruthy()
  })

  it('renders the HeaderList component with acronym sorted desc column', async () => {
    render(
      <SortProvider>
        <HeaderList />
      </SortProvider>
    )
    const button = screen.getByText(/acronym/i)
    await act(() => fireEvent.press(button))
    await act(() => fireEvent.press(button))
    const acronym = within(button.parent!.parent!.parent)
    expect(acronym.queryByText('▼')).toBeTruthy()
  })

  it('renders the HeaderList component without acronym sorted column', async () => {
    render(
      <SortProvider>
        <HeaderList />
      </SortProvider>
    )
    const button = screen.getByText(/acronym/i)
    await act(() => fireEvent.press(button))
    await act(() => fireEvent.press(button))
    await act(() => fireEvent.press(button))
    expect(screen.queryByText('▼')).toBeFalsy()
    expect(screen.queryByText('▲')).toBeFalsy()
  })

  it('renders the HeaderList component with p/vp sort asc column', async () => {
    render(
      <SortProvider>
        <HeaderList />
      </SortProvider>
    )
    const button = screen.getByText(/pvp/i)!.parent!.parent!.parent
    await act(() => fireEvent.press(button))
    const acronym = within(button)
    expect(acronym.queryByText('▲')).toBeTruthy()
  })

  it('renders the HeaderList component with dy sorted asc column', async () => {
    render(
      <SortProvider>
        <HeaderList />
      </SortProvider>
    )
    const button = screen.getByText(/dy/i)!.parent!.parent!.parent
    await act(() => fireEvent.press(button))
    const acronym = within(button)
    expect(acronym.queryByText('▲')).toBeTruthy()
  })

  it('renders the HeaderList component with created sorted asc column', async () => {
    render(
      <SortProvider>
        <HeaderList />
      </SortProvider>
    )
    const button = screen.getByText(/created/i)!.parent!.parent!.parent
    await act(() => fireEvent.press(button))
    const acronym = within(button)
    expect(acronym.queryByText('▲')).toBeTruthy()
  })

  it('renders the HeaderList component with created sorted asc column before click in acronym', async () => {
    render(
      <SortProvider>
        <HeaderList />
      </SortProvider>
    )
    const button1 = screen.getByText(/acronym/i)!.parent!.parent!.parent
    const button2 = screen.getByText(/created/i)!.parent!.parent!.parent
    await act(() => fireEvent.press(button1))
    await act(() => fireEvent.press(button2))
    const acronym = within(button1)
    const created = within(button2)
    expect(created.queryByText('▲')).toBeTruthy()
    expect(acronym.queryByText('▲')).toBeFalsy()
    expect(acronym.queryByText('▼')).toBeFalsy()
  })
})
