module.exports = {
  arrowParens: 'avoid',
  bracketSameLine: true,
  bracketSpacing: false,
  singleQuote: true,
  trailingComma: 'none',
  endOfLine: 'lf',
  semi: false,
  printWidth: 130,
  jsxSingleQuote: true
}
