import React, {useEffect} from 'react'
import {useCallback} from 'react'
import {FlatList, Text, TouchableOpacity} from 'react-native'
import {HeaderList} from './component/HeaderList'
import {Fii, Item} from './component/Item'
import {useSort} from './context/SortContext'
import {getAllFii} from './service/FundamentusHandle'
import {getAllFii as getAllFiiB3} from './service/B3ServiceHandle'
import {getAllFii as getAllFiiClube} from './service/ClubeFiiServiceHandle'
import {getAllFii as getAllSI} from './service/StatusInvestServiceHandle'
import styles from './styles'
import {MinMax, useFilter} from './context/FilterContext'
import {useItem} from './context/ItemContext'

type MainComponentProps = {}

const MainComponent = ({}: MainComponentProps) => {
  const {fiis, setFiis} = useItem()
  const renderItem = useCallback(({item}: {item: Fii}) => <Item {...item} />, [])
  const keyStractor = useCallback((item: Fii) => item.acronym, [])
  const itemLayout = useCallback(
    (_: Fii[] | null | undefined, index: number) => ({
      length: 60,
      offset: 60 * index,
      index: index
    }),
    []
  )
  const {sortFunction} = useSort()
  const {pvp, dy, liquidity, created, acronym, setFiltered, filtered} = useFilter()

  useEffect(() => {
    // initial state, fill filter as default
    setFiltered(fiis)
  }, [fiis, setFiltered])

  useEffect(() => {
    let tmps = [...fiis]
    function filterValue(field: keyof Fii, value: MinMax | string) {
      tmps = tmps.filter(e => {
        const obj_val = e[field]
        if (obj_val) {
          if (typeof value === 'string') {
            return obj_val.toString().toUpperCase().includes(value.toUpperCase())
          } else {
            return obj_val > value.min && obj_val < value.max
          }
        }
        return false
      })
    }
    if (pvp) {
      filterValue('pvp', pvp)
    }
    if (dy) {
      filterValue('dy', dy)
    }
    if (liquidity) {
      filterValue('liquidity', liquidity)
    }
    if (created) {
      filterValue('created', created)
    }
    if (acronym) {
      filterValue('acronym', acronym)
    }
    setFiltered(tmps)
  }, [acronym, created, dy, fiis, liquidity, pvp, setFiltered])

  async function getFiis() {
    const arr = [getAllFiiB3(), getAllFii(), getAllFiiClube(false), getAllSI(false)]
    Promise.all(arr)
      .then(res => {
        const b3List = res[0]
        const fundList = res[1]
        const clubeList = res[2]
        const statusList = res[3]
        const mix = b3List.map(b3 => {
          const idxF = fundList.findIndex((f: Fii) => f.acronym === b3.acronym)
          const fund = idxF !== -1 ? fundList.slice(idxF, idxF + 1)[0] : undefined
          const idxC = clubeList.findIndex((c: Fii) => c.acronym === b3.acronym)
          const club = idxC !== -1 ? clubeList.slice(idxC, idxC + 1)[0] : undefined
          const idxS = statusList.findIndex((c: Fii) => c.acronym === b3.acronym)
          const stat = idxS !== -1 ? statusList.slice(idxS, idxS + 1)[0] : undefined
          return {
            acronym: b3.acronym,
            name: b3.name,
            liquidity: fund?.liquidity,
            pvp: stat?.pvp || fund?.pvp,
            dy: stat?.dy || fund?.dy,
            created: club?.created
          }
        })
        setFiis(mix.sort(sortFunction))
      })
      .catch(console.error)
  }

  return (
    <>
      <FlatList
        data={filtered.sort(sortFunction)}
        renderItem={renderItem}
        contentContainerStyle={styles.listContent}
        keyExtractor={keyStractor}
        ListHeaderComponent={<HeaderList />}
        stickyHeaderIndices={[0]}
        ListHeaderComponentStyle={styles.header}
        maxToRenderPerBatch={12}
        getItemLayout={itemLayout}
        ListEmptyComponent={<Text style={styles.empty}>Nothing to show</Text>}
      />
      <TouchableOpacity style={styles.button} activeOpacity={0.7} onPress={getFiis}>
        <Text style={styles.buttonLabel}>Recarregar</Text>
      </TouchableOpacity>
    </>
  )
}

export {MainComponent}
