import React from 'react'
import {View} from 'react-native'
import {useSort} from '../../context/SortContext'
import {Fii} from '../Item'
import {FilterCell, Sort} from './FilterCell'
import styles from './styles'

const HeaderList: React.FC = () => {
  const {field, setField, order, setOrder} = useSort()

  const getOrder = (f: keyof Fii): Sort | undefined => {
    if (f === field) {
      return order
    }
    return undefined
  }

  function toggleOrder(name: keyof Fii | undefined) {
    let ord
    if (order === undefined || name !== field) {
      ord = Sort.ASC
    } else if (order === Sort.ASC) {
      ord = Sort.DESC
    }
    setOrder(ord)
    setField(name)
  }

  return (
    <View style={[styles.header]}>
      <FilterCell
        style={[styles.headerText, styles.cell]}
        label={['acronym', 'name']}
        ordered={getOrder('acronym')}
        onPress={() => toggleOrder('acronym')}
      />
      <FilterCell
        style={[styles.headerText, styles.cell]}
        label={['pvp']}
        ordered={getOrder('pvp')}
        onPress={() => toggleOrder('pvp')}
      />
      <FilterCell
        style={[styles.headerText, styles.cell]}
        label={['dy']}
        ordered={getOrder('dy')}
        onPress={() => toggleOrder('dy')}
      />
      <FilterCell
        style={[styles.headerText, styles.cell]}
        label={['created']}
        ordered={getOrder('created')}
        onPress={() => toggleOrder('created')}
      />
    </View>
  )
}

export {HeaderList}
