import {StyleSheet} from 'react-native'
import {colors, fonts} from '../../styles'

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 25
  },
  headerText: {
    color: colors.foregroundPrimary,
    textTransform: 'uppercase',
    fontSize: fonts.sm,
    textAlign: 'center',
    justifyContent: 'center',
    textAlignVertical: 'center'
  },
  cell: {
    width: '100%',
    textAlign: 'center'
  },
  containerCell: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 70,
    height: '100%'
  },
  cellText: {
    flex: 1,
    height: '100%',
    justifyContent: 'center'
  },
  cellSortContainer: {
    justifyContent: 'center'
  },
  cellSortText: {
    fontSize: 8,
    flex: 1,
    textAlignVertical: 'center',
    color: colors.foregroundPrimary
  }
})

export default styles
