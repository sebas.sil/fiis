import React from 'react'
import {Text, TouchableOpacity, TouchableWithoutFeedbackProps, View} from 'react-native'
import {Fii} from '../Item'
import styles from './styles'

enum Sort {
  ASC,
  DESC
}

type FilterCellProps = {
  label: Array<keyof Fii>
  style?: object[]
  ordered?: Sort
} & TouchableWithoutFeedbackProps

const FilterCell: React.FC<FilterCellProps> = ({label, style, ordered, ...others}) => {
  return (
    <TouchableOpacity {...others}>
      <View style={styles.containerCell}>
        <View style={styles.cellText}>
          {label.map((e, i) => (
            <Text key={i} style={style}>
              {e}
            </Text>
          ))}
        </View>
        <View style={styles.cellSortContainer}>
          {ordered !== undefined && <Text style={styles.cellSortText}>{ordered === Sort.ASC ? '▲' : '▼'}</Text>}
        </View>
      </View>
    </TouchableOpacity>
  )
}

export {FilterCell, Sort}
