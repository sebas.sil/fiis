import React from 'react'
import {Text, View} from 'react-native'
import styles from './styles'

type Fii = {
  acronym: string
  name: string
  liquidity?: number
  pvp?: number
  dy?: number
  created?: Date
}

const Item: React.FC<Fii> = ({acronym, name, pvp, dy, created}) => {
  return (
    <View style={styles.container}>
      <View style={styles.logo}>
        <Text style={[styles.itemText, styles.acronym]}>{acronym}</Text>
        <Text style={[styles.itemText, styles.name]} numberOfLines={2} ellipsizeMode='tail'>
          {name}
        </Text>
      </View>
      <Text style={[styles.cell, styles.itemText]}>{pvp?.toFixed(2)}</Text>
      <Text style={[styles.cell, styles.itemText]}>{dy !== undefined && dy?.toFixed(2) + '%'}</Text>
      <Text style={[styles.cell, styles.itemText]}>{created?.toLocaleDateString()}</Text>
    </View>
  )
}

export type {Fii}
export {Item}
