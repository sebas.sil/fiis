import {StyleSheet} from 'react-native'
import {colors, fonts, spaces} from '../../styles'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    gap: spaces.sm,
    height: 50,
    backgroundColor: colors.backgroundLight,
    marginVertical: spaces.sm / 2,
    justifyContent: 'space-between'
  },
  logo: {
    width: 50,
    height: 50,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  acronym: {
    textTransform: 'uppercase',
    height: 25,
    width: 50,
    textAlignVertical: 'center',
    textAlign: 'center'
  },
  name: {
    fontSize: fonts.sm,
    height: 25,
    width: 50,
    textTransform: 'uppercase',
    textAlign: 'center'
  },
  cell: {
    width: 50,
    textAlign: 'center'
  },
  itemText: {
    fontSize: fonts.md
  }
})

export default styles
