import {LabelProps} from '@ptomasroos/react-native-multi-slider'
import React from 'react'
import {View, Text} from 'react-native'
import styles from './styles'

type LabelBase = {
  position: boolean
  value: string
}
const LabelBase = ({position, value}: LabelBase) => {
  return (
    <View
      style={[
        styles.sliderLabelContainer,
        // eslint-disable-next-line react-native/no-inline-styles
        position ? {left: -45} : {right: -45}
      ]}>
      <Text style={styles.sliderLabelText}>{value}</Text>
    </View>
  )
}

const textTransformer = (value: number, type?: 'date' | 'number' | undefined) => {
  if (type === 'date') {
    const dt = new Date(value)
    return [dt.getFullYear() % 1000, dt.toLocaleDateString(undefined, {month: 'short'})].join('-')
  }
  return value.toLocaleString(undefined, {useGrouping: true, maximumFractionDigits: 1})
}

export default function SliderCustomLabel({oneMarkerValue, twoMarkerValue}: LabelProps, type: 'date' | 'number' | undefined) {
  return (
    <View>
      <LabelBase position={true} value={textTransformer(+oneMarkerValue, type)} />
      <LabelBase position={false} value={textTransformer(+twoMarkerValue, type)} />
    </View>
  )
}
