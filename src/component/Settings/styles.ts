import {StyleSheet} from 'react-native'
import {colors, dimensions, spaces} from '../../styles'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.backgroundLight,
    paddingVertical: spaces.sm / 2,
    justifyContent: 'flex-start',
    borderWidth: 2,
    borderColor: 'red'
  },
  title: {
    textTransform: 'capitalize'
  },
  slider: {
    width: dimensions.fullWidth / 2
  },
  sliderLine: {
    backgroundColor: colors.foregroundPrimary
  },
  sliderMarker: {
    backgroundColor: colors.foregroundPrimary
  },
  sliderLabel: {
    textTransform: 'uppercase',
    textAlign: 'center'
  },
  containerSlider: {
    marginTop: 30
  },
  sliderLabelContainer: {
    position: 'absolute',
    justifyContent: 'center',
    top: 4,
    width: 50,
    height: 50
  },
  sliderLabelText: {
    textAlign: 'center',
    lineHeight: 50,
    flex: 1,
    fontSize: 8
  }
})

export default styles
