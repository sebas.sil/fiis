import React, {useEffect, useState} from 'react'
import {Text, View} from 'react-native'
import {useItem} from '../../context/ItemContext'
import {SettingField} from './SettingField'
import styles from './styles'

type SettingsProps = {}

const Settings = ({}: SettingsProps) => {
  const {fiis} = useItem()
  const [values, setValues] = useState<number[]>([0, 1, 0, 1]) //minDy, maxDy, minPvp, maxPvp

  useEffect(() => {
    let maxDy: number | undefined
    let minDy: number | undefined
    let maxPvp: number | undefined
    let minPvp: number | undefined

    fiis.forEach(e => {
      if (!maxDy || (e.dy && e.dy > maxDy)) {
        maxDy = e.dy
      }
      if (!minDy || (e.dy && e.dy < minDy)) {
        minDy = e.dy
      }

      if (!maxPvp || (e.pvp && e.pvp > maxPvp)) {
        maxPvp = e.pvp
      }
      if (!minPvp || (e.pvp && e.pvp < minPvp)) {
        minPvp = e.pvp
      }
    })
    if (minPvp && minDy && maxDy && maxPvp) {
      setValues([minDy, maxDy, minPvp, maxPvp])
    }
  }, [fiis])

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Settings</Text>
      <SettingField
        minValue={new Date('2020-01-01').getTime()}
        maxValue={new Date().getTime()}
        label={'created'}
        type='date'
        step={2592000000}
      />
      <SettingField minValue={values[0]} maxValue={values[1]} label='dy' type='number' step={0.2} />
      <SettingField minValue={0} maxValue={1000} label='liquidity' type='number' step={1} />
      <SettingField minValue={values[2]} maxValue={values[3]} label='pvp' type='number' step={0.2} />
    </View>
  )
}

export {Settings}
