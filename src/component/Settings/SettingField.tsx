import React from 'react'
import {Text, View} from 'react-native'
import MultiSlider from '@ptomasroos/react-native-multi-slider'
import styles from './styles'
import {dimensions} from '../../styles'
import SliderCustomLabel from './SliderCustomLabel'
import {useFilter} from '../../context/FilterContext'
import {Fii} from '../Item'

type SettingFieldProps = {
  minValue: number
  maxValue: number
  label: keyof Fii
  initalLowValue?: number
  initalHigh?: number
  type?: 'date' | 'number'
  step?: number
}

const SettingField = ({
  minValue,
  maxValue,
  label,
  initalLowValue = minValue,
  initalHigh = maxValue,
  type,
  step
}: SettingFieldProps) => {
  const {setField} = useFilter()

  return (
    <View style={styles.containerSlider}>
      <Text style={styles.sliderLabel}>{label}</Text>
      <MultiSlider
        markerStyle={styles.sliderMarker}
        sliderLength={dimensions.fullWidth / 2}
        min={minValue}
        selectedStyle={styles.sliderLine}
        max={maxValue}
        values={[initalLowValue, initalHigh]}
        customLabel={e => SliderCustomLabel(e, type)}
        enableLabel={true}
        step={step}
        onValuesChange={(values: number[]) => setField(label, {min: values[0], max: values[1]})}
      />
    </View>
  )
}

export {SettingField}
