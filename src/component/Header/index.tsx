import React from 'react'
import {DrawerToggleButton} from '@react-navigation/drawer'
import {Text, View, TextInput} from 'react-native'
import styles from './styles'
import {useFilter} from '../../context/FilterContext'
import {useItem} from '../../context/ItemContext'

const Header = () => {
  const {fiis} = useItem()
  const {filtered, setField} = useFilter()
  return (
    <View style={styles.container}>
      <DrawerToggleButton tintColor='white' />
      <TextInput
        style={styles.search}
        placeholder='Busque pela tag'
        placeholderTextColor='white'
        onChangeText={value => setField('acronym', value)}
      />
      <View style={styles.filterLabelContainer}>
        <Text style={styles.filterLabel}>Mostrando</Text>
        <Text style={styles.filterLabel}>
          {filtered.length}/{fiis.length}
        </Text>
      </View>
    </View>
  )
}

export {Header}
