import {StyleSheet} from 'react-native'
import {colors, fonts, spaces} from '../../styles'

const styles = StyleSheet.create({
  container: {
    height: 38,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: spaces.sm,
    backgroundColor: colors.backgroundDark
  },
  search: {
    borderRadius: spaces.xsm,
    borderColor: 'gray',
    borderWidth: 1,
    flex: 1,
    height: 30,
    fontSize: fonts.smd,
    paddingHorizontal: spaces.xsm,
    paddingVertical: 0
  },
  filterLabelContainer: {
    width: 50
  },
  filterLabel: {
    textAlign: 'center',
    fontSize: fonts.smd
  }
})

export default styles
