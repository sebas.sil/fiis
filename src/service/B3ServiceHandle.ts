import {Fii} from '../component/Item'
import {Buffer} from 'buffer'

type B3Response = {
  segment: string
  acronym: string
  fundName: string
  companyName: string
  cnpj: string
}

async function getAllFii(enabled: boolean = true): Promise<Fii[]> {
  if (enabled) {
    //const startTime = performance.now()
    const URL = 'https://sistemaswebb3-listados.b3.com.br/fundsProxy/fundsCall/GetListedFundsSIG/'
    const options = {typeFund: 7, pageNumber: 1, pageSize: 1000}
    const base64encoded = Buffer.from(JSON.stringify(options)).toString('base64')

    return fetch(URL + base64encoded)
      .then(response => response.json())
      .then(e => convertContent(e.results))
      .catch(e => {
        throw Error('cant parse Fundamentus: ' + e)
      })
    //.finally(() => console.log(`Call to b3 took ${Math.ceil(performance.now() - startTime)} milliseconds`))
  }
  return new Promise(s => s([]))
}

const convertContent = (content: B3Response[]): Fii[] =>
  content.map(e => ({
    acronym: e.acronym + 11,
    name: e.fundName.trim()
  })) as Fii[]

export {getAllFii}
