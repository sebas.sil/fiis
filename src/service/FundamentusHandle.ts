import {Fii} from '../component/Item'

async function getAllFii(enabled: boolean = true): Promise<Fii[]> {
  if (enabled) {
    //const startTime = performance.now()
    return fetch('https://www.whateverorigin.org/get?url=http%3A%2F%2Fwww.fundamentus.com.br%2Ffii_resultado.php')
      .then(response => response.text())
      .then(e => convertContent(e))
      .catch(e => {
        throw Error('cant parse Fundamentus: ' + e)
      })
    //.finally(() => console.log(`Call to fundamentus took ${Math.ceil(performance.now() - startTime)} milliseconds`))
  }
  return new Promise(s => s([]))
}

const convertContent = (content: string) => {
  const text = content.replace(/\\n/g, '')
  const re = /<tr class=\\"\w*\\">(.+?)<\/tr>/g
  const arr = text.split(re).filter(e => e.trim())
  arr.shift()
  arr.pop()
  const rec =
    /<span .*title=\\"(.*)\\"><a .*?>(.+?)<\/a>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>.*?<td>(.*?)<\/td>/
  return arr.map((e, i) => {
    var match = rec.exec(e)
    if (match) {
      return {
        acronym: match[2],
        name: match[1]
          .split('-')[0]
          .trim()
          .replace(/FUNDO DE INVESTIMENTO IMOBILI[ÁA]RIO/i, 'FII'),
        pvp: parseFloat(match[6].replace(/,/, '.')),
        dy: parseFloat(match[5].replace(/,/, '.').replace(/%/, '')),
        liquidity: parseFloat(match[8].replace(/\./, ''))
      }
    }
    return {acronym: '?' + i}
  }) as unknown as Fii[]
}

export {getAllFii}
