import {Fii} from '../component/Item'

async function getAllFii(enable: boolean = true): Promise<Fii[]> {
  if (enable) {
    //const startTime = performance.now()
    const URL = 'https://www.clubefii.com.br/fundo_imobiliario_lista'

    return fetch(URL)
      .then(response => response.text())
      .then(convertContent)
      .catch(e => {
        throw Error('cant parse ClubeFii: ' + e)
      })
    //.finally(() => console.log(`Call to clubeFii took ${Math.ceil(performance.now() - startTime)} milliseconds`))
  }
  return new Promise(s => s([]))
}

const convertContent = (content: string): Fii[] => {
  const text = content.replace(/\n/g, '')
  const re = /<tr .*?>/g
  const arr = text.split(re)
  arr.shift()
  arr.pop()
  const rec =
    /.*?<td>.*?<a .*?>(.+?)<\/a>.*?<\/td>.*?<a .*?>(.+?)<\/a>.*?<\/td>.*?<span .*?>(.+?)<\/span>.*?<span .*?>(.+?)<\/span>.*?<span .*?>(.+?)<\/span>.*?<a .*?>(.+?)<\/a>.*?<a .*?>(.+?)<\/a>.*?<a .*?>(.+?)<\/a>.*?<a .*?>(.+?)<\/a>/
  return arr.map((e, i) => {
    var match = rec.exec(e)
    if (match) {
      const dt = Date.parse(match[6].trim().split('/').reverse().join('-'))
      return {
        acronym: match[1].trim(),
        name: match[2].trim(),
        created: isNaN(dt) ? undefined : new Date(dt)
      }
    }
    //console.error('match not found')
    return {acronym: '?' + i}
  }) as unknown as Fii[]
}

export {getAllFii}
