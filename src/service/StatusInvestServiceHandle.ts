import {Fii} from '../component/Item'

type WEOResponse = {
  contents: string
  content_type: string
  http_code: number
}

type SIResponse = {
  companyId: number
  companyName: string
  cota_cagr: number
  dividend_cagr: number
  dy: number
  gestao: string
  lastdividend: number
  liquidezmediadiaria: number
  numerocotas: number
  numerocotistas: number
  p_vp: number
  patrimonio: number
  percentualcaixa: number
  price: number
  ticker: string
  valorpatrimonialcota: number
}

async function getAllFii(enabled: boolean = true): Promise<Fii[]> {
  if (enabled) {
    //const startTime = performance.now()
    var params = {
      Segment: '',
      Gestao: '',
      my_range: '0;20',
      dy: {Item1: null, Item2: null},
      p_vp: {Item1: null, Item2: null},
      percentualcaixa: {Item1: null, Item2: null},
      numerocotistas: {Item1: null, Item2: null},
      dividend_cagr: {Item1: null, Item2: null},
      cota_cagr: {Item1: null, Item2: null},
      liquidezmediadiaria: {Item1: null, Item2: null},
      patrimonio: {Item1: null, Item2: null},
      valorpatrimonialcota: {Item1: null, Item2: null},
      numerocotas: {Item1: null, Item2: null},
      lastdividend: {Item1: null, Item2: null}
    }

    const URL =
      'https://www.whateverorigin.org/get?url=' +
      encodeURIComponent(
        'https://statusinvest.com.br/category/advancedsearchresult?search=' + JSON.stringify(params) + '&CategoryType=2'
      )
    return fetch(URL)
      .then(response => response.json())
      .then(convertContent)
      .catch(e => {
        return Promise.reject('cant parse statusinvest: ' + e)
      })
    //.finally(() => console.log(`Call to b3 took ${Math.ceil(performance.now() - startTime)} milliseconds`))
  }
  return new Promise(s => s([]))
}

const convertContent = (content: WEOResponse): Fii[] => {
  const converted: SIResponse[] = JSON.parse(content.contents)
  return converted.map(e => ({
    acronym: e.ticker,
    name: e.companyName,
    dy: e.dy,
    pvp: e.p_vp
  })) as Fii[]
}

export {getAllFii}
