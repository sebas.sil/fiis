import React, {createContext, useContext, useState} from 'react'
import {Fii} from '../component/Item'

enum Sort {
  ASC,
  DESC
}

interface SortProviderProps {
  children: React.ReactNode
}

interface SortContextData {
  order: Sort | undefined
  field: keyof Fii | undefined
  setOrder: (order: Sort | undefined) => void
  setField: (order: keyof Fii | undefined) => void
  sortFunction: (obj1: Fii, obj2: Fii) => -1 | 0 | 1
}

const SortProvider: React.FC<SortProviderProps> = ({children}) => {
  const [order, setOrder] = useState<Sort | undefined>()
  const [field, setField] = useState<keyof Fii | undefined>()

  function sortFunction(a: Fii, b: Fii) {
    if (field) {
      if (a[field] < b[field]) {
        return order ? 1 : -1
      } else if (a[field] > b[field]) {
        return order ? -1 : 1
      } else if (a[field] !== undefined && b[field] === undefined) {
        return -1
      } else if (a[field] === undefined && b[field] !== undefined) {
        return 1
      }
    }
    return 0
  }

  return <SortContext.Provider value={{order, setOrder, field, setField, sortFunction}}>{children}</SortContext.Provider>
}

const SortContext = createContext<SortContextData>({} as SortContextData)

const useSort = () => useContext(SortContext)

export {useSort, SortProvider}
