import React, {useState} from 'react'
import {createContext, useContext} from 'react'
import {Fii} from '../component/Item'

type ItemContextData = {
  fiis: Fii[]
  setFiis: (items: Fii[]) => void
}

type ItemProviderProps = {
  children: React.ReactNode
}

const ItemProvider: React.FC<ItemProviderProps> = ({children}) => {
  const [fiis, setFiis] = useState<Fii[]>([])
  return <ItemContext.Provider value={{fiis, setFiis}}>{children}</ItemContext.Provider>
}

const ItemContext = createContext<ItemContextData>({} as ItemContextData)

const useItem = () => useContext(ItemContext)
export {ItemProvider, useItem}
