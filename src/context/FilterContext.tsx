import React, {useState} from 'react'
import {createContext, useContext} from 'react'
import {Fii} from '../component/Item'

type MinMax = {
  min: number
  max: number
}

type FilterContextData = {
  pvp?: MinMax
  dy?: MinMax
  liquidity?: MinMax
  created?: MinMax
  acronym?: string
  setField: (field: keyof Fii, values: MinMax | string) => void
  filtered: Fii[]
  setFiltered: (fiis: Fii[]) => void
}

type FilterProviderProps = {
  children: React.ReactNode
}

const FilterContext = createContext<FilterContextData>({} as FilterContextData)

const FilterProvider: React.FC<FilterProviderProps> = ({children}) => {
  // TODO change to useReducer
  const [pvp, setPvp] = useState<MinMax>()
  const [dy, setDy] = useState<MinMax>()
  const [liquidity, setLiquidity] = useState<MinMax>()
  const [created, setCreated] = useState<MinMax>()
  const [acronym, setAcronym] = useState<string>()
  const [filtered, setFiltered] = useState<Fii[]>([])

  function setField(field: keyof Fii, values: MinMax | string) {
    switch (field) {
      case 'dy':
        setDy(values as MinMax) //FIXME
        break
      case 'pvp':
        setPvp(values as MinMax) //FIXME
        break
      case 'liquidity':
        setLiquidity(values as MinMax) //FIXME
        break
      case 'created':
        setCreated(values as MinMax) //FIXME
        break
      case 'acronym':
        setAcronym(values as string) //FIXME
        break
    }
  }

  return (
    <FilterContext.Provider value={{pvp, dy, liquidity, created, acronym, setField, filtered, setFiltered}}>
      {children}
    </FilterContext.Provider>
  )
}

const useFilter = () => useContext(FilterContext)

export type {MinMax}
export {useFilter, FilterProvider}
