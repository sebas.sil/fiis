import React from 'react'
import {SafeAreaView} from 'react-native'
import {SortProvider} from './context/SortContext'
import {MainComponent} from './MainComponent'
import styles from './styles'
import {createDrawerNavigator} from '@react-navigation/drawer'
import {NavigationContainer} from '@react-navigation/native'
import {Settings} from './component/Settings'
import {FilterProvider} from './context/FilterContext'
import {ItemProvider} from './context/ItemContext'
import {Header} from './component/Header'

const App = () => {
  const Drawer = createDrawerNavigator()

  const Home = () => {
    return (
      <SafeAreaView style={[styles.container]}>
        <SortProvider>
          <MainComponent />
        </SortProvider>
      </SafeAreaView>
    )
  }

  return (
    // add initial values (max/min) to filter
    <NavigationContainer>
      <ItemProvider>
        <FilterProvider>
          <Drawer.Navigator
            drawerContent={() => <Settings />}
            initialRouteName='Home'
            screenOptions={{header: Header, swipeEdgeWidth: 50}}>
            <Drawer.Screen name='Home' component={Home} />
          </Drawer.Navigator>
        </FilterProvider>
      </ItemProvider>
    </NavigationContainer>
  )
}

export {App}
