import {Dimensions, StyleSheet} from 'react-native'

const colors = {
  backgroundLight: '#121214',
  backgroundDark: '#000',
  foregroundPrimary: '#FFCD1E'
}

const dimensions = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}

const spaces = {
  /**
   * 5
   */
  xsm: 5,
  /**
   * 10
   */
  sm: 10
  /**
   * 20
   */
  //  md: 20,
  /**
   * 30
   */
  //  lg: 30,
  /**
   * 40
   */
  //  xl: 40
}

const fonts = {
  /**
   * 8
   */
  sm: 8,
  /**
   * 10
   */
  smd: 10,
  /**
   * 14
   */
  md: 14
  /**
   * 32
   */
  //  lg: 32,
  /**
   * 64
   */
  //  xl: 64,
  /**
   * Poppins-Regular
   */
  //  primary: 'Poppins-Regular',
  /**
   * Poppins-Bold
   */
  //  primaryBold: 'Poppins-Bold',
  /**
   * Archivo-Regular
   */
  //  secondary: 'Archivo-Regular',
  /**
   * Archivo-Bold
   */
  //  secondaryBold: 'Archivo-Bold'
}

const styles = StyleSheet.create({
  container: {
    padding: spaces.sm,
    flex: 1,
    backgroundColor: colors.backgroundDark,
    justifyContent: 'space-between'
  },
  header: {
    height: 25,
    backgroundColor: colors.backgroundLight
  },
  button: {
    height: 48,
    backgroundColor: colors.foregroundPrimary,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonLabel: {
    color: colors.backgroundDark,
    textTransform: 'uppercase',
    fontWeight: 'bold'
  },
  empty: {
    flexGrow: 1,
    textAlign: 'center',
    textAlignVertical: 'center'
  },
  listContent: {
    flexGrow: 1
  }
})

export {dimensions, colors, spaces, fonts}
export default styles
